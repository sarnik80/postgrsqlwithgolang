package main

import (
	"database/sql"
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
)

type Post struct {
	Id      int
	Content string
	Author  string
}

var Db *sql.DB

func init() {
	var err error

	connectionSTR := "postgres://root:j06ar8KkODkxhOon0y2LEe2y@arthur.iran.liara.ir:30135/s?sslmode=disable"
	Db, err = sql.Open("postgres", connectionSTR)

	if err != nil {
		panic(err)
	}
}

func main() {

	http.HandleFunc("/", create)
	http.HandleFunc("/retrieve", retrieve)
	http.HandleFunc("/update", update)
	http.HandleFunc("/delete", delete)
	http.HandleFunc("/posts", posts)
	http.ListenAndServe(":8080", nil)

}

func create(w http.ResponseWriter, r *http.Request) {

	content := r.FormValue("content")
	author := r.FormValue("author")

	post := Post{

		Content: content,
		Author:  author,
	}

	stmt := "insert into posts (content , author) values ($1 , $2) returning id"

	dt, err := Db.Prepare(stmt)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer dt.Close()

	err = Db.QueryRow(stmt, post.Content, post.Author).Scan(&post.Id)

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("id of your post", post.Id)
}

/*  we chain the QueryRow method and Scan mwthod to copy the value of the
returned results on the empty post struct.


*/
func retrieve(w http.ResponseWriter, r *http.Request) {

	id := r.FormValue("id")

	post := Post{}

	statement := "select id, content , author  from posts where id = $1"

	err := Db.QueryRow(statement, id).Scan(&post.Id, &post.Content, &post.Author)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("content : ", post.Content)
}

/*

	* we don't need to scan the returned results
	therefore using Exsc mwthod on the global variable Db is
	much faster  ...

	it returns  'sql.Result'  and an 'error'  is much faster

*/

func update(w http.ResponseWriter, r *http.Request) {

	content := r.FormValue("content")
	author := r.FormValue("author")
	id := r.FormValue("id")

	stmt := "update posts set content = $2 , author = $3  where id = $1"
	_, err := Db.Exec(stmt, id, content, author)

	if err != nil {
		fmt.Println(err)
	}

}

func delete(w http.ResponseWriter, r *http.Request) {

	id := r.FormValue("id")

	queryString := "delete from posts where id = $1"
	_, err := Db.Exec(queryString, id)

	if err != nil {
		fmt.Println(err)
	}

}

/*

	we use Query method on the sql.DB struct which returns a Rows interface !
	Rows interface is an iterator !

	we can call Next() method on it and it'll return sql.row


*/

func posts(w http.ResponseWriter, r *http.Request) {

	limit := r.FormValue("limit")

	posts := []Post{}
	stmt := "select *  from posts limit $1"

	rows, err := Db.Query(stmt, limit)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer rows.Close()

	for rows.Next() {

		post := Post{}

		err = rows.Scan(&post.Id, &post.Content, &post.Author)

		if err != nil {

			fmt.Println(err)

			return
		}

		posts = append(posts, post)

	}

	fmt.Println("size : ", len(posts))

}
